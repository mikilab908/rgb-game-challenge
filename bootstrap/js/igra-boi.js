$('document').ready(function () {
  let colorsArr = [];
  let spanColor;
  let easy = false;
  resetGame();

  function randomSpanColor() {
    spanColor = colorsArr[Math.floor(Math.random() * colorsArr.length)];
  }

  function randomColor() {
    colorsArr = [];

    $('.square').each(function () {
      if ($(this).is('.hard') && easy == true) {
        $(this).hide();
      } else {
        let color01 = Math.floor(Math.random() * 255);
        let color02 = Math.floor(Math.random() * 255);
        let color03 = Math.floor(Math.random() * 255);

        let squareColor = `rgb(${color01}, ${color02}, ${color03})`;

        colorsArr.push(squareColor);

        $(this).css('background-color', squareColor);
      }
    });
  }

  function resetGame() {
    $('.square').show();
    randomColor();
    randomSpanColor();

    $('#message').empty();
    $('#reset').text('NEW COLORS');
    $('#colorDisplay').text(spanColor);
    $('h1').css('background-color', 'steelblue');
  }

  $('#reset').on('click', function () {
    resetGame();
  });

  $('#easy').on('click', function () {
    $(this).addClass('selected');
    $('#hard').removeClass('selected');
    easy = true;
    resetGame();
    $('.hard').hide();
  });

  $('#hard').on('click', function () {
    $(this).addClass('selected');
    $('#easy').removeClass('selected');
    $('.hard').show();
    easy = false;
    resetGame();
  });

  $('.square').on('click', function () {
    if ($(this).css('background-color') == spanColor) {
      $('#reset').text('PLAY AGAIN…');
      if (easy) {
        $('.easy').css('background-color', spanColor);
      } else {
        $('.square').css('background-color', spanColor);
      }
      $('h1').css('background-color', spanColor);
      $('#message').text('You won!');
    } else {
      $(this).fadeOut();
      $('#message').text('Try Again!');
    }
  });
});
